const runDbDef = require('../database/scripts/run-db-def');

async function main(){
    await runDbDef();
    process.exit();
}


main().catch(err => {
    console.error(err);
    process.exit(1);
});