process.env.MASTER_PASSWORD = process.env.MASTER_PASSWORD || Math.random().toString().slice(2, 9);
process.env.SECRET = process.env.SECRET || Math.random().toString().slice(2, 9);
const express = require('express');
const compression = require('compression');
const cookieParser = require('cookie-parser')
const formidable = require('formidable');
const helmet = require('helmet');
const localtunnel = require('localtunnel');

const { ServerError, manageLogs } = require('litlib');
const { appLog, startLogging, stopLogging, connectSocket } = require('@app/utils');

manageLogs();
global.ServerError = ServerError;

const mountRoutes = require('./routes');
const app = express();
/** @type {import('http').Server} */
const server = require('http').Server(app);

connectSocket(server);

const { peerSetup } = require('./utils/peer-actions');

server.on('listening', async () => {
    const port = server.address().port;
    const tunnel = await localtunnel({ port });
    peerSetup(server, tunnel.url);

    try {
        await startLogging();
    } catch (err) {
        console.error('Could not start logging.');
        console.error(err);
    }
    console.sysLog = appLog;
});
server.on('close', async () => {
    await stopLogging();
});


/**
 * Force redirect to https in production
 */
if (process.env.NODE_ENV === 'production') {
    app.use(function (req, res, next) {
        if (req.headers['x-forwarded-proto'] !== 'https') {
            return res.redirect(['https://', req.get('Host'), req.url].join(''));
        }
        return next();
    });
}



// CORS
app.use((req, res, next) => {
    console.log(req.headers.origin);
    if (/localhost/.test(req.headers.origin)) {
        res.header("Access-Control-Allow-Origin", req.headers.origin);
    } else {
        res.header("Access-Control-Allow-Origin", process.env.COORDINATOR_URI);
    }
    res.header("Vary", 'Origin');
    res.header("Access-Control-Allow-Headers", '*');
    res.header("Access-Control-Allow-Methods", 'POST');
    res.header("Access-Control-Allow-Credentials", true);
    if (req.method === 'OPTIONS') {
        res.sendStatus(200);
    } else {
        next();
    }
})
app.use(cookieParser(process.env.SECRET));
app.use(helmet());
app.use(compression());
app.use((req, res, next) => {
    let form = new formidable.IncomingForm({
        encoding: 'utf-8',
        multiples: true,
        keepExtension: true
    });
    form.once('error', err => {
        console.error('Formidable error: ', err);
        res.sendStatus(500);
    });

    const parsedJSONFields = {};
    const deletedKeys = [];
    form.on('field', (key, val) => {
        if (/{}$/.test(key)) {
            parsedJSONFields[key.replace(/{}$/, '')] = JSON.parse(val);
            deletedKeys.push(key);
        }
    });

    form.parse(req, (err, fields, files) => {
        for (key of deletedKeys) {
            delete fields[key];
        }
        fields = { ...fields, ...parsedJSONFields };

        Object.assign(req, { fields, files });
        next();
    });
});



/**
 * App Routes
 */
mountRoutes(app);


/**
 * Default Handler
 */
app.get('*', (_req, res) => {
    res.sendStatus(403);
});


/**
 * Error Handler
 */
app.use((err, req, res, next) => {
    if (/^AE/.test(err.code)) {
        err = new ServerError(422, err.message, err);
    }
    else if (err.name === 'ValidationError') {
        const text = err.details.map(e => e.message).join(', ');
        err = new ServerError(422, text, err);
    }
    else if (err.code === '23505') {
        const text = `Duplicate entry. ${err.detail}`;
        err = new ServerError(422, text, err);
    }

    if (res.headersSent) {
        return next(err);
    }

    console.error(`Error in [${req.method}] ${req.path}`);
    if (err.name !== 'ServerError') err = new ServerError(err);

    console.error({ ...err });

    err.status = err.status || 500;
    if (err.text) {
        res.status(err.status).send(err.text);
    } else {
        res.sendStatus(err.status);
    }
});


module.exports = server;