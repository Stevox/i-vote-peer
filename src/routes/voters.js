const express = require('express');
const { castVoteAsPeer } = require('../model/entities/voters');
const { wrapAsync } = require('litlib/utils');


const router = express.Router();

module.exports = app => {
    app.use('/voters', router);
}

router.post('/election/vote', wrapAsync(async (req, res) => {
    await castVoteAsPeer({ ...req.fields });
    // TODO Log new vote occurence
    res.sendStatus(204);
}));