/**
 * @param {import('express').Express} app
 */
module.exports = app => {
    require('./voters')(app);
}