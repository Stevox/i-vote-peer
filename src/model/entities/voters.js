const { snakeToCamelCase, ServerError } = require('litlib');
const Joi = require('joi');
const { Db } = require('litlib/ldb');
const {
    castVoteSchema
} = require('../schemas/voter-schema');
const { broadcastVoteToPeers } = require('@app/utils');
const db = new Db();



// Holds unconfirmed votes to prevent trying to confirm votes not processed yet
const castVotes = {};
// Holds confirmed votes to be updated on db after processing
const confirmedVotes = {};

async function castVoteAsPeer({ voterHash, electionId, castDate, pollVotes }) {
    ({
        voterHash, electionId, castDate, pollVotes
    } = await Joi.attempt(arguments[0], castVoteSchema));

    const q1 = {
        text: `INSERT INTO votes_unconfirmed 
            (voter_hash, election_id, candidate_id, poll_id, cast_date ) VALUES `,
        values: [electionId]
    }
    
    let addComma = '';
    for(let vote of pollVotes){
        let i = q1.values.length;
        q1.values.push(voterHash, vote.candidateId, vote.pollId, castDate);
        q1.text += `${addComma}($${++i}, $1, $${++i}, $${++i}, $${++i})`;
        addComma = ', ';
    }
    q1.text += ' RETURNING *';

    
    const r1 = await db.execute(q1);
    if (!r1.rows[0]) {
        throw new ServerError(401, 'Invalid vote');
    }

    const resp = [];
    const votes = r1.rows;
    for(let vote of votes){
        vote = snakeToCamelCase(vote);
        resp.push(vote);
        castVotes[vote.voteHash] = true;
        if(confirmedVotes[vote.voteHash]){
            confirmVote(vote);
        }
    }

    broadcastVoteToPeers(resp);
    return resp;
}


async function confirmVote({ voteHash }) {
    if(!castVotes[voteHash]){
        confirmedVotes[voteHash] = true;
        return;
    }
    
    const qn = [{
        text: `SELECT * FROM votes_unconfirmed WHERE vote_hash = $1 LIMIT 1`,
        values: [voteHash]
    }, {
        text: `INSERT INTO votes_confirmed 
            (voter_hash, election_id, candidate_id, poll_id, cast_date) VALUES 
            (#voter_hash#, #election_id#, #candidate_id#, #poll_id#, #cast_date#) RETURNING *`
    }];

    const [, r2] = await db.execute(qn);
    if (!r2.rows[0]) {
        throw new ServerError(401, 'Invalid vote hash');
    }

    delete confirmedVotes[voteHash];
    delete castVotes[voteHash];
    
    return;
}



module.exports = {
    castVoteAsPeer, confirmVote
}