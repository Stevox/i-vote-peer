const { BaseJoi } = require('./schema-ext');

const Joi = BaseJoi

module.exports.castVoteSchema = Joi.object({
    electionId: Joi.number().required(),
    voterHash: Joi.string().required(), 
    castDate: Joi.date().required(), 
    pollVotes: Joi.array().items(Joi.object({
        pollId: Joi.number().required(),
        candidateId: Joi.number().required(),
    })).required(), 
});
