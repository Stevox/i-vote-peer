const { connectSocketClient, getSocketIo } = require('./socket');
const Joi = require('joi');

const peers = {};

/** @param {import('http').Server} server */
async function peerSetup(server, uri) {
    if (!uri) {
        const { address, port } = server.address();
        if (data.family === 'IPv6') {
            uri = `http://[${address}]:${port}`;
        } else {
            uri = `http://${address}:${port}`;
        }
    }
    console.info({ localAddress: uri });
    const io = getSocketIo();
    io.origins = '*';

    io.on('connect', socket => {
        // Every connection is considered a peer.
        socket.join('peers');
        const uri = socket.handshake.headers.host;
        console.info({ info: 'New peer client connected', uri });
    });


    // Connect to coordinator server
    const coordinatorUri = process.env.COORDINATOR_URI;
    if (coordinatorUri) {
        const client = await connectToPeerServer({ uri: coordinatorUri });
        const data = { uri };
        data.electionId = process.env.ELECTION_ID;
        peerReg();
        client.on('reconnect', peerReg);
        function peerReg() {
            client.emit('peer-registration', data, ({ secret, error }) => {
                if (error) {
                    console.error(error);
                    return process.exit();
                }
                process.env.PEER_SECRET = secret;
            });
        }
        client.on('new-peer', connectToPeerServer);
        client.on('test', console.log)
    } else {
        throw new Error('Coordinator URI not defined');
    }
}


const MIN_PC = .6;
async function connectToPeerServer(peer) {
    Joi.assert(peer, Joi.object({ uri: Joi.string() }).unknown());
    if (peers[peer.uri]) {
        console.info({ info: `Peer ${peer.uri} is already connected` });
        return;
    }
    peers[peer.uri] = peer;
    const { confirmVote } = require('../model/entities/voters');
    const client = await connectSocketClient(peer.uri);
    console.info({ info: 'Connected to new peer server', uri: peer.uri });
    const confirmationCount = {};
    client.on('test', console.log)
    client.on('vote-confirmation', (votes) => {
        Joi.assert(votes, Joi.array().items(Joi.object({ voteHash: Joi.string() }).unknown()));
        const MIN_CONFIRMATIONS = Math.ceil(Object.keys(peers).length * MIN_PC);
        for (let vote of votes) {
            confirmationCount[vote.voteHash] = confirmationCount[vote.voteHash] || 0;
            const count = ++confirmationCount[vote.voteHash];
            console.log('Confirmation', { count, vote, MIN_CONFIRMATIONS });
            if (count >= MIN_CONFIRMATIONS) {
                confirmVote(vote);
                delete confirmationCount[vote.voteHash];
            }
        }
    });
    return client;
}


function broadcastVoteToPeers(data) {
    const io = getSocketIo();
    io.to('peers').emit('vote-confirmation', data);
}



module.exports = {
    broadcastVoteToPeers, peerSetup
}