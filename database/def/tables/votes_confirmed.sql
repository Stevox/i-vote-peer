CREATE TABLE votes_confirmed (
  vote_id BIGSERIAL PRIMARY KEY,
  voter_hash TEXT NOT NULL,
  election_id BIGINT NOT NULL,
  candidate_id BIGINT NOT NULL,
  poll_id BIGINT NOT NULL,
  cast_date TIMESTAMP NOT NULL DEFAULT now(),
  vote_hash TEXT NOT NULL,
  block_hash TEXT NOT NULL, -- specific to the peer
  UNIQUE (voter_hash, election_id, poll_id) -- Single vote per poll
);


CREATE OR REPLACE FUNCTION hash_new_vote_confirmed() RETURNS trigger AS $$
  BEGIN
    IF NEW.vote_hash IS NOT null THEN
      PERFORM prevent_forbidden_action();
    END IF;
    NEW.vote_hash = (SELECT substring(digest(
        NEW.voter_hash || NEW.election_id || NEW.candidate_id || NEW.poll_id, 'sha1'
      )::TEXT from 3));

    -- Block hash for quick checking tampering
    IF NEW.block_hash IS NOT null THEN
      PERFORM prevent_forbidden_action();
    END IF;
    NEW.block_hash = (SELECT substring(digest(
        NEW.vote_hash || (SELECT COALESCE((
          SELECT block_hash FROM votes_confirmed WHERE vote_id = NEW.vote_id - 1
        ),'')), 'sha1')::TEXT from 3));
    
    RETURN NEW;
  END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER hash_new_vote_confirmed_trigg BEFORE INSERT ON votes_confirmed 
  FOR EACH ROW EXECUTE PROCEDURE hash_new_vote_confirmed();



CREATE TRIGGER prevent_vote_confirmed_delete_trigg BEFORE DELETE OR UPDATE ON votes_confirmed 
  FOR EACH ROW EXECUTE PROCEDURE prevent_forbidden_action();