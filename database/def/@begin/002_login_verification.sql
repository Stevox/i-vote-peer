CREATE FUNCTION set_login_verification_request_expiry()
RETURNS trigger AS $$
  BEGIN
    SELECT now() + INTERVAL '5min' INTO NEW.verification_expiry;
    RETURN NEW;      
  END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION validate_login_verification()
RETURNS trigger AS $$
  BEGIN
    IF (SELECT OLD.verification_expiry::TIMESTAMP < now()) THEN
      RAISE SQLSTATE 'AE001' 
        USING MESSAGE = 'Reset link expired';
    END IF;
    NEW.verification_expiry = NULL;
    RETURN NEW;      
  END;
$$ LANGUAGE plpgsql;
