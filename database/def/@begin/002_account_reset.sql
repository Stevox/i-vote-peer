CREATE FUNCTION set_reset_request_expiry()
RETURNS trigger AS $$
  BEGIN
    SELECT now() + INTERVAL '15min' INTO NEW.reset_request_expiry;
    RETURN NEW;      
  END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION validate_account_password_reset()
RETURNS trigger AS $$
  BEGIN
    IF (SELECT OLD.reset_request_expiry::TIMESTAMP < now()) THEN
      RAISE SQLSTATE 'AE001' 
        USING MESSAGE = 'Reset link expired';
    END IF;
    NEW.reset_request_expiry = NULL;
    RETURN NEW;      
  END;
$$ LANGUAGE plpgsql;