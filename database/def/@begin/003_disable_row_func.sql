/**
 * Executed by a BEFORE DELETE trigger on tables with an 'is_active' column.
 * If effective row has existing FK dependencies, is_active is set to false.
 *  Elsewise, the row is deleted.
 */
CREATE OR REPLACE FUNCTION disable_row_func() RETURNS trigger AS $$
  DECLARE
    ri RECORD;
    has_deps BOOLEAN;
  BEGIN
    FOR ri IN 
      SELECT * FROM get_dependent_tables(TG_TABLE_NAME) dep_t 
    LOOP
      EXECUTE format(
        'SELECT ($1).%I = ANY ( %L )', 
        ri.foreign_column_name, ri.column_values
      ) INTO has_deps USING OLD;
      IF has_deps THEN
        EXECUTE format(
          'UPDATE %I SET is_active = false WHERE %I = $1.%2$I', 
          TG_TABLE_NAME, ri.foreign_column_name
        ) USING OLD;
        OLD = NULL;
        EXIT;
      END IF;
    END LOOP;
    RETURN OLD;
  END;
$$ LANGUAGE plpgsql;
