/**
 * Gets tables with FK dependency to ref_table_name 
 *  as well as an array of dependent values.
 */
CREATE OR REPLACE FUNCTION get_dependent_tables(ref_table_name TEXT)
  RETURNS TABLE (
    table_schema TEXT, constraint_name TEXT, table_name TEXT, column_name TEXT, column_values TEXT,
    foreign_table_name TEXT, foreign_column_name TEXT, row_count INT
  )
AS $$
  BEGIN
    FOR 
      table_schema, constraint_name, table_name, column_name, column_values,
      foreign_table_name, foreign_column_name, row_count 
    IN 
      SELECT 
          tc.table_schema::TEXT, tc.constraint_name::TEXT, 
          tc.table_name::TEXT, kcu.column_name::TEXT, null,
          ccu.table_name::TEXT
          AS foreign_table_name, ccu.column_name::TEXT AS foreign_column_name, 0
        FROM information_schema.table_constraints tc
          JOIN information_schema.key_column_usage kcu ON tc.constraint_name = kcu.constraint_name
          JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_name = tc.constraint_name
        WHERE constraint_type = 'FOREIGN KEY' AND ccu.table_name = $1
    LOOP
      EXECUTE format(
        'SELECT array_agg(%I), count(*) FROM %I', 
        column_name,
        table_name
      ) INTO column_values, row_count; 
      RETURN NEXT;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;
