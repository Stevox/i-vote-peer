const server = require('../../src');
const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const expect = chai.expect;

module.exports = function () {
    describe('/voters', () => {

        it('POST [/election/vote] - Should save vote');
        
        it('POST [/election/vote] - Should save vote only once');
    });
}