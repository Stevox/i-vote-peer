process.env.NODE_ENV = 'test';
const runDbDef = require('../database/scripts/run-db-def');

const chai = require('chai');
const chaiHttp = require('chai-http');

process.env.VERBOSITY = 0;
chai.use(chaiHttp);


before(() => {
    it('should set up the testing environment', async () => {
        await runDbDef();
    });
});