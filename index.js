const server = require('./src');
const port = process.env.PORT;

server.listen(port, async () => {
    console.log('Server running on port:', server.address().port);
});


function terminate(code, reason) {
    // Exit function
    const exit = () => {
        process.exit(code)
    }

    return (err, promise) => {
        console.log(`Server shutting down. Reason: ${reason}`);

        
        if (err && err instanceof Error) {
            console.error(err.message, err.stack)
        }

        server.close(exit)
        setTimeout(exit, 1000).unref()
    }
}




process.on('uncaughtException', terminate(1, 'Unexpected Error'))
process.on('unhandledRejection', terminate(1, 'Unhandled Promise'))
process.on('SIGTERM', terminate(0, 'SIGTERM'))
process.on('SIGINT', terminate(0, 'SIGINT'))