## Installation of the I-Vote peer server

Minimum requirements are similar to the main server.


### Installation Instructions

1. Create the database and user to be used by the server.
1. Clone the repository (link in the [README](./README.md)) and follow any specific instructions detailed in the README.
1. Open your terminal and navigate into the cloned i-vote-peer folder.
1. Run the command ```npm run init-db``` to initialize the database.
1. Run the command ```npm start``` to start the server.
1. A unique uri will be provisioned to the peer server to add it to the blockchain network.
1. No further action necessary.
